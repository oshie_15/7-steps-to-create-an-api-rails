class ForestsController < ApplicationController
  def index
    @forests = Forest.all

    render json: @forests
  end

  def show
    @forest = Forest.find(params[:id])

    render json: @forest
  end

  def create
    @forest = Forest.new(forest_params)

    if @forest.save
      render json: @forest
    else
      render json: { errors: @forest.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def update
    @forest = Forest.find(params[:id])
    @forest.update(forest_params)

    render json: "#{@forest.name} has been updated!"
  end

  def destroy
    @forest = Forest.find(params[:id])
    @forest.destroy

    render json: "#{@forest.name} has been deleted!"
  end


  private

  def forest_params
    params.permit(:name, :state)
  end
end
