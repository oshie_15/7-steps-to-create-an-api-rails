class TrailsController < ApplicationController
  def index
    @trails = Trail.all

    render json: @trails
  end

  def show
    @trail = Trail.find(params[:id])

    render json: @trail
  end

  def create
    @trail = Trail.new(trail_params)

    if @trail.save
      render json: @trail
    else
      render json: { errors: @trail.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def update
    @trail = Trail.find(params[:id])
    @trail.update(trail_params)

    render json: "#{@trail.name} has been updated!"
  end

  def destroy
    @trail = Trail.find(params[:id])
    @trail.destroy

    render json: "#{@trail.name} has been deleted!"
  end

  private

  def trail_params
    params.permit(:name, :state, :miles, :forest_id)
  end
end
